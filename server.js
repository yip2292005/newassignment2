var restify = require("restify");
var firebase = require("firebase");
var request = require("request");
// create the Server
var server = restify.createServer();

server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());
//firebase information
 var config = {
    apiKey: "AIzaSyBvqCF4LZqiq2O53oq-05mqJ224GXOczAc",
    authDomain: "assignement-8b16c.firebaseapp.com",
    databaseURL: "https://assignement-8b16c.firebaseio.com",
    projectId: "assignement-8b16c",
    storageBucket: "assignement-8b16c.appspot.com",
    messagingSenderId: "918944229828"
  };
  firebase.initializeApp(config);
//connect database
var database = firebase.database();

//openweather api
var api ="http://api.openweathermap.org/data/2.5/forecast?id=1818209&APPID=321603fd9ef5197b09592ca14040b344"
//googleleapis
var google = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=AIzaSyD60JbkI19pQ-DLcDhZvnNlmKfKaxniNPQ"


//server port for 8080
server.listen(process.env.PORT || 8080, function(rep, err) {
  //server running, show server running
  if(!err )
  {
    console.log('server running')
    //request http client
    request(api, function (err, res, body){
   if(!err && res.statusCode == 200){
     //json parase
       var testdata = JSON.parse(body);
        var list = testdata["list"][0]['weather'][0]["main"];
        //var weather = list["weather"];
            //var googlemap = JSON.parse(test);
       console.log("Today: weather" + "  " +list)
       //console.log(googlemap)
       //console.log(testdata)
   }
   request(google, function(err, res, body) {
       if (!err && res.statusCode == 200){
           var googlemap = JSON.parse(body)
           //var rating = googlemap[0]["rating"];
           var result = googlemap["results"][0]["rating"]
           console.log(result)
       }
   })
 })
       
  }else{
    //if sever not work, show server error
    console.log('server error')
  }
});

//Get method, get the user data
server.get('/showuser', function(req, res, next) {
  var id = req.query.id;
  if (id==null){
    id="";
  }
  var result = new Promise(function(resolve, reject) {
    //Ref to user
    var userRef = 'users/' + id
    var newuserRef = database.ref(userRef);
    newuserRef.once('value').then(function(snap) {
      var json = snap.val();
      var size = snap.numChildren();
      //database has data, show the data, size and successful to get data
      if (json != null) {
          size = 1;
        resolve({
          success: true,
          list: json,
          size: size,
          message:"successful to get data"
        });
      }
      //the database has not any data, show the userid cannot find 
      else {
        resolve({
          success: false,
          message: "The userid cannot found"
        });
      }
    });
  });
  
  result.then(function(value) {
    res.send(value);
    res.end();
  })
})
//Post method, insert the data to database
server.post('/Insertuser', function(req, res, next) {
    var result = new Promise(function(resolve, reject) {
        if (req.headers['content-type'] != 'application/json') {
            resolve({
                success: false,
                message: "fail to insert the database"
            });
        }
        else {

            var json = req.body;
            var id = req.query.newuserID;
            var Userid;
            var UserRef;
            var IdRef = 'Id/';

            database.ref('Id/').once('value').then(function(snap) {
                var IdInDatabase = snap.val();
                if (IdInDatabase != null) {
                    Userid = IdInDatabase;
                    if (Object.prototype.toString.call(json) === '[object Object]') {
                        var newuserId = IdInDatabase + 1;
                        UserRef = "users/" + newuserId;
                        firebase.database().ref(UserRef).set(json);
                        firebase.database().ref(IdRef).set(newuserId);
                    }
                    else {
                       
                        for (i = 0; i < json.length; i++) {
                            var newuserId = i + Userid;
                            UserRef = "users/" + newuserId;
                            firebase.database().ref(UserRef).set(json[i]);
                            IdInDatabase++;
                        }
                        firebase.database().ref(IdRef).set(IdInDatabase);
                    }
                }

                else {

                    if (Object.prototype.toString.call(json) === '[object Object]') {
                        var newuserId = 0;
                        UserRef = "users/" + newuserId;
                        firebase.database().ref(UserRef).set(json);
                        newuserId++;
                        firebase.database().ref(IdRef).set(newuserId);
                    }
                    else {
                        Userid = 0;
                        for (i = 0; i < json.length; i++) {
                            UserRef = "users/" + i;
                            firebase.database().ref(UserRef).set(json[i]);
                            Userid++;
                        }
                        firebase.database().ref(IdRef).set(Userid);
                    }
                }
            });

            resolve({
                success: true,
                message: "successful to insert the database",
                "TotaljsonSize": json.length,
                "Userid": Userid,
                list:json

            });
        }
    });
    result.then(function(value) {
        res.send(value);
        res.end();
    });
});



//put method, update the user infromation
server.put("/updateuser", function(req, res, next){
  var result = new Promise(function(resolve, reject){
    if (req.headers['content-type'] == 'application/json'){
      var id = req.query.newuserID;
      var json = req.body;
      if (id != null && json != null){

        //create the refernce for database and update the data with json
        firebase.database().ref("users/" + id).update(json);
        
        resolve({
          //sucessful to update the database infomartion
          success: true,
          message:"success, update the database information",
          id : id,
          update: json
        });
      }else{
        //cannot update the database, show the message
        resolve({
          success: false,
        message:"fail to update database"
        });
      }
    }
  });
    result.then(function(value){
    res.send(value);
    res.end();
  });
});
//del method, remove the data
server.del('/removeuser', function(req, res, next){
  var result = new Promise(function (resolve, reject){
    //query the newuserID
    var newuserID = req.query.newuserID;
    if (newuserID != null){
      //create the ref 
      var userRef = "users/" + newuserID;
    
      firebase.database().ref(userRef).remove();
      resolve({
        //sucessful remove the database, show the message
        success: true,
        newuserID : newuserID,
        message : "sucessful to remove database"
      });
    }else{
      //if error, show fail to remove database
      resolve({
        success :false,
        message : "fail to remove database"
      });
    }
  });
  
  result.then(function(value){
    res.send(value);
    res.end();
  });
});